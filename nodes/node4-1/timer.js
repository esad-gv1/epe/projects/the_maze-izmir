function startTimer() {
    let count = 100; // Valeur de départ du timer
    const timerDiv = document.getElementById('timer'); // Récupération de la div

    const intervalId = setInterval(() => {
        timerDiv.textContent = count; // Mise à jour de la div avec la valeur actuelle
        count--; // Décrémentation du compteur
        
        if (count < 0) {
            clearInterval(intervalId); // Arrêt du timer lorsque le compteur est inférieur à 0
        }
    }, 600); // Intervalle de 1000 ms (1 seconde)
}


var paragraph = document.getElementById('clickableParagraph');
var soundEffect = document.getElementById('soundEffect');

// Définir le volume à 1.0 s'il dépasse cette valeur après multiplication par 3
soundEffect.volume = Math.min(1.0, soundEffect.volume * 5);

// Ajouter un événement de clic sur la balise <p>
paragraph.addEventListener('click', function() {
    soundEffect.play();
});


/* 

document.addEventListener("DOMContentLoaded", function () {
    var timerElement = document.getElementById("timer");

    var observerOptions = {
        root: null, // par défaut, l'élément racine est le viewport
        rootMargin: '0px',
        threshold: 0.1 // déclenchement lorsque 10% de la div est visible
    };

    function handleIntersection(entries, observer) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                onTimerVisible();
                // Si vous souhaitez arrêter l'observation après la première intersection, décommentez la ligne suivante :
                // observer.unobserve(entry.target);
            }
        });
    }

    var observer = new IntersectionObserver(handleIntersection, observerOptions);
    observer.observe(timerElement);
});

*/