# EPE

## The Maze

Content source : *Fear and design : how can design impact on fear? how can design raise questions about fear?* Jenny Bergström, 2007, Nordes 2007: Design Inquiries, 27-30 May, University of Arts, Craft, and Design, Stockholm, Sweden.

The story text and illustrations are originals and were created during the workshop.

# General concept

A visual novel can be explored by the user (the player, the reader). The path that has been choosen into the maze can lead to various ending. Each steps is memorized as every node of the story is associated to a part of the article about *Fear and design*. When one end is reached, the paginated version of the article parts can be printed.

This project requiered specific developements to make the story more easy to write. A lightweight visual novel engine has been written for the occasion and can be reused for various other stories with the same interactive principles. The main technical idea is to organise several StoryNodes inside a StoryContext that will handel each node media inside an iFrame. The engine itself can be found in the `js` folder.